FastAPI based service for bike sharing prediction
===================================================

## Setup: using `poetry` (recommended) or `pip`
1. Poetry
- install [pyenv](https://github.com/pyenv/pyenv#installation) and [poetry](https://python-poetry.org/docs/#installation)
```commandline
pyenv local 3.<9|10|11>.x
poetry env use 3.<9|10|11>.x
poetry install
poetry shell  # or `source $(poetry env info -p)/bin/activate`
```
2. Pip
- use some python3 of version ^3.9
```commandline
python3 -m venv venv
source venv/bin/activate
pip install --upgrade pip setuptools wheel
pip install .
# pip doesn't support poetry dev dependencies, so please install them manually
pip install ipykernel pre-commit
```

### Install jupyter notebook kernel 
- to use the kernel you need jupyter server running locally (you can install jupyter in any local python)
```commandline
python -m ipykernel install --user --name bike_demand_py3<9|10|11>
```

### Install pre-commit hooks (optional)
```commandline
pre-commit autoupdate
pre-commit install
```

## Run Application
1. Simple version
```commandline
python fastapi_bike_sharing/app/simple_app.py
```
2. Main version
```commandline
python -m fastapi_bike_sharing
```
3. Using Docker
```commandline
 docker build --tag fastapi-bike-sharing:latest .
 docker run -p 8077:8077 localhost/fastapi-bike-demand:latest
```
