ARG USERNAME=bike-demand-service-user
ARG USER_UID=2000
ARG USER_GID=$USER_UID
ARG WORKDIR=/srv/www/

FROM python:3.9-slim as builder
ARG USERNAME
ARG USER_UID
ARG USER_GID
ARG WORKDIR
WORKDIR $WORKDIR
RUN groupadd --gid $USER_GID $USERNAME
RUN useradd --uid $USER_UID --gid $USER_GID -m $USERNAME
COPY poetry.lock .
COPY pyproject.toml .
RUN apt-get update -y
# install python packaging managers
RUN pip install -U pip poetry
RUN poetry config virtualenvs.create false
# install python packages
RUN poetry install --only main --compile
# cleanup
RUN rm poetry.lock
RUN poetry cache clear pypi --all
RUN pip uninstall -y poetry
RUN pip cache purge
RUN apt-get clean autoclean
RUN apt-get autoremove --yes
RUN rm -rf /var/lib/{apt,dpkg,cache,log}/
RUN rm -rf /var/lib/apt/lists/*

# for: 1) smaller size 2) improved build performance 3) simplier dependency management 4) enhanced security
FROM python:3.9-slim as runtime
ARG USERNAME
ARG WORKDIR
WORKDIR $WORKDIR
COPY --from=builder / /
COPY . $WORKDIR
USER $USERNAME
CMD ["python", "-m", "fastapi_bike_sharing"]
