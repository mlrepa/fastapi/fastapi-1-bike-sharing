"""Demand prediction service."""
import typing

import pylru

from fastapi_bike_sharing.app import data_models
from fastapi_bike_sharing.model.demand_model import BikeDemandModel
from fastapi_bike_sharing.settings import APP_SETTINGS, FEATURE_EXTRACTOR_PATH


class RequestIdentifier(typing.NamedTuple):
    target_type: data_models.TargetCol
    params: data_models.BikeDemandParams


_DEMAND_CACHE: typing.Final[dict[RequestIdentifier, int]] = (
    pylru.lrucache(APP_SETTINGS.cache_size) if APP_SETTINGS.cache_size > 0 else {}
)


class BikeDemandService:
    __slots__ = ("_input_request", "_demand_engine")

    _input_request: data_models.BikeDemandRequest
    _demand_engine: BikeDemandModel

    def prepare(self: "BikeDemandService", request_payload: data_models.BikeDemandRequest) -> "BikeDemandService":
        """Initialize engine."""
        self._input_request = request_payload
        self._demand_engine = BikeDemandModel(
            request_payload.target_type,
            feature_extractor_path=FEATURE_EXTRACTOR_PATH,
        )
        return self

    def predict_demand(self: "BikeDemandService") -> list[int]:
        demand_prediction: list[int] = [-1] * len(self._input_request.params)

        index_to_predict: list[int] = []
        params_to_predict: list[data_models.BikeDemandParams] = []
        request_ids: list[RequestIdentifier] = []
        idx: int
        params: data_models.BikeDemandParams
        for idx, params in enumerate(self._input_request.params):
            if (
                request_id := RequestIdentifier(target_type=self._input_request.target_type, params=params)
            ) in _DEMAND_CACHE:
                demand_prediction[idx] = _DEMAND_CACHE[request_id]
            else:
                index_to_predict.append(idx)
                params_to_predict.append(params)
                request_ids.append(request_id)

        if params_to_predict:
            model_predictions: list[int] = self._demand_engine.predict(params=params_to_predict)
            for idx, prediction, request_id in zip(index_to_predict, model_predictions, request_ids):
                demand_prediction[idx] = prediction
                _DEMAND_CACHE[request_id] = prediction

        return demand_prediction
