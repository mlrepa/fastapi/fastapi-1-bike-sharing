"""Features and feature extraction."""
from __future__ import annotations
import enum
import pickle
import typing

import numpy as np
import pandas as pd
from loguru import logger

from fastapi_bike_sharing.app.data_models import BikeDemandParams, Col, NewCol, TargetCol


if typing.TYPE_CHECKING:
    import pathlib


FEATURE_COLUMN_ORDER: typing.Final[list[Col | NewCol]] = [
    NewCol.DAY_OF_YEAR,
    Col.HOUR,
    Col.HOLIDAY,
    Col.WEEKDAY,
    Col.WORKING_DAY,
    Col.WEATHER,
    Col.TEMPERATURE,
    Col.FEELING_TEMPERATURE,
    Col.HUMIDITY,
    Col.WIND_SPEED,
]

CATEGORICAL_FEATURES: typing.Final[set[Col]] = {Col.WEEKDAY, Col.WEATHER}

TARGET_COLUMNS: typing.Final[set[TargetCol]] = {
    TargetCol.CASUAL_COUNT,
    TargetCol.REGISTERED_COUNT,
    TargetCol.TOTAL_COUNT,
}


REQUEST_TO_COL_MAP: typing.Final[dict[str, Col]] = {
    "date": Col.DATE,
    "hour": Col.HOUR,
    "is_holiday": Col.HOLIDAY,
    "weather_type": Col.WEATHER,
    "temperature_celsius": Col.TEMPERATURE,
    "feeling_temperature_celsius": Col.FEELING_TEMPERATURE,
    "humidity": Col.HUMIDITY,
    "wind_speed": Col.WIND_SPEED,
}

COL_TYPES: typing.Final[dict[Col, str]] = {
    Col.DATE: "datetime64[ns]",
    Col.HOUR: "int",
    Col.HOLIDAY: "int",
    Col.WORKING_DAY: "int",
    Col.WEATHER: "int",
    Col.TEMPERATURE: "float",
    Col.FEELING_TEMPERATURE: "float",
    Col.HUMIDITY: "float",
    Col.WIND_SPEED: "float",
}


class BikeFeatureExtractor:
    encoded_columns: list[str] | None

    def __init__(
        self,
        *,
        feature_column_order: typing.Sequence[str],
        categorical_features: typing.Collection[str],
        target_columns: typing.Collection[str],
    ) -> None:
        self.feature_column_order = feature_column_order
        self.categorical_features = categorical_features
        self.target_columns = target_columns
        self.encoded_columns = None

    @staticmethod
    def _cast_df_types(df: pd.DataFrame) -> pd.DataFrame:
        return df.astype(COL_TYPES, errors="ignore")

    def _transform_target(self, df: pd.DataFrame) -> pd.DataFrame:
        return df[list(self.target_columns)].astype(int)

    @staticmethod
    def _add_new_columns(df: pd.DataFrame) -> pd.DataFrame:
        df[NewCol.DAY_OF_YEAR] = df[Col.DATE].dt.month - 1 + df[Col.DATE].dt.day / df[Col.DATE].dt.days_in_month
        if not all(col in df.columns for col in NewCol):
            raise RuntimeError(f"Some columns from {NewCol} are not present in {df.columns}")
        return df

    @staticmethod
    def _transform_categorical_to_cyclic(
        df: pd.DataFrame,
        *,
        col_name: str,
        max_val: float,
        min_val: float = 0.0,
    ) -> pd.DataFrame:
        radian_series: pd.Series = 2 * np.pi * (df[col_name] - min_val) / (max_val - min_val)
        df[f"{col_name}_sin"] = np.sin(radian_series)
        df[f"{col_name}_cos"] = np.cos(radian_series)
        return df.drop(columns=[col_name])

    def _transform_features(self, df: pd.DataFrame) -> pd.DataFrame:
        df = df.copy()
        df = self._add_new_columns(df)
        df = df[self.feature_column_order]

        for col, max_val in [(NewCol.DAY_OF_YEAR, 12.0), (Col.HOUR, 24.0)]:
            df = self._transform_categorical_to_cyclic(df, col_name=col, max_val=max_val)
        return df

    def _fit_transform(self, df: pd.DataFrame) -> pd.DataFrame:
        df = self._transform_features(df)
        one_hot_features: pd.DataFrame = self._fit_one_hot_encoding(df)
        return pd.concat([df.drop(columns=self.categorical_features), one_hot_features], axis=1)

    def _transform(self, df: pd.DataFrame) -> pd.DataFrame:
        df = self._transform_features(df)
        one_hot_features: pd.DataFrame = self._transform_one_hot_encoding(df)
        return pd.concat([df.drop(columns=self.categorical_features), one_hot_features], axis=1)

    def _fit_one_hot_encoding(self, df: pd.DataFrame) -> pd.DataFrame:
        cat_features: list[str] = sorted(self.categorical_features)
        one_hot_features: pd.DataFrame = pd.get_dummies(
            df[cat_features],
            columns=cat_features,
            prefix=cat_features,
            drop_first=True,
            dtype=int,
        )
        self.encoded_columns = one_hot_features.columns
        return one_hot_features

    def _transform_one_hot_encoding(self, df: pd.DataFrame) -> pd.DataFrame:
        if self.encoded_columns is None:
            raise RuntimeError("One hot encoding should be fitted first.")
        cat_features: list[str] = list(self.categorical_features)
        one_hot_features: pd.DataFrame = pd.get_dummies(
            df[cat_features],
            columns=cat_features,
            prefix=cat_features,
            dtype=int,
        )
        return one_hot_features.reindex(columns=self.encoded_columns, fill_value=0)

    @staticmethod
    def _enum_column_to_string(column: str | enum.Enum) -> str:
        return str(column.value) if isinstance(column, enum.Enum) else column

    def fit_transform(self, df: pd.DataFrame) -> pd.DataFrame:
        df = df.copy()
        df = self._cast_df_types(df)
        features: pd.DataFrame = self._fit_transform(df)
        targets: pd.DataFrame = self._transform_target(df)
        df = pd.concat([features, targets], axis=1)
        return df.rename(columns=self._enum_column_to_string)

    def transform(self, df: pd.DataFrame) -> pd.DataFrame:
        df = df.copy()
        df = self._cast_df_types(df)
        df = self._transform(df)
        return df.rename(columns=self._enum_column_to_string)

    @staticmethod
    def _build_dataset_from_request(params: list[BikeDemandParams]) -> pd.DataFrame:
        row_dict: dict[int, dict[str, typing.Any]] = {}
        for i, data in enumerate(params):
            row_dict[i] = {col: getattr(data, attr_name) for attr_name, col in REQUEST_TO_COL_MAP.items()}
        return pd.DataFrame.from_dict(row_dict, orient="index")

    @staticmethod
    def _enrich_request_dataset(df: pd.DataFrame) -> pd.DataFrame:
        df[Col.WEEKDAY] = (df[Col.DATE].astype("datetime64[ns]").dt.weekday + 1) % 7
        df[Col.WORKING_DAY] = ~(df[Col.HOLIDAY] | df[Col.WEEKDAY].isin((0, 6)))
        return df

    def transform_request(self, params: list[BikeDemandParams]) -> pd.DataFrame:
        df: pd.DataFrame = self._build_dataset_from_request(params)
        df = self._enrich_request_dataset(df)
        return self.transform(df)

    def save(self, file_path: pathlib.Path) -> None:
        if self.encoded_columns is None:
            logger.warning("Encoder wasn't fitted. Transform is not available.")
        with file_path.open("wb") as f:
            pickle.dump(self, f)

    @classmethod
    def load(cls: BikeFeatureExtractor, file_path: pathlib.Path) -> BikeFeatureExtractor:
        with file_path.open("rb") as file:
            feature_extractor: BikeFeatureExtractor = pickle.load(file)  # noqa: S301
            return feature_extractor
