"""Model for bike demand prediction."""
import pathlib
import typing

import joblib
import numpy as np
import pandas as pd

from fastapi_bike_sharing import settings
from fastapi_bike_sharing.app import data_models
from fastapi_bike_sharing.model.features import BikeFeatureExtractor


class _BikeDemandModel(typing.Protocol):
    def predict(self, features: pd.DataFrame) -> np.ndarray:
        ...


class BikeDemandModel:
    __slots__ = ("_feature_extractor", "_model")

    _feature_extractor: BikeFeatureExtractor
    _model: _BikeDemandModel

    def __init__(self, target_type: data_models.TargetCol, *, feature_extractor_path: pathlib.Path) -> None:
        self._feature_extractor = BikeFeatureExtractor.load(feature_extractor_path)

        model_path: pathlib.Path = settings.build_model_path(target_type)
        self._model = joblib.load(model_path)

    @staticmethod
    def post_process(predictions: np.ndarray) -> list[int]:
        return np.rint(predictions.clip(min=0)).astype(int).tolist()

    def predict(self, params: list[data_models.BikeDemandParams]) -> list[int]:
        features: pd.DataFrame = self._feature_extractor.transform_request(params)
        predictions: np.ndarray = self._model.predict(features)
        return self.post_process(predictions)

    def batch_predict(self, data_batch: pd.DataFrame) -> list[int]:
        features: pd.DataFrame = self._feature_extractor.transform(data_batch)
        predictions: np.ndarray = self._model.predict(features)
        return self.post_process(predictions)
