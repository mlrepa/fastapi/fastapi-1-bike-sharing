"""Application server."""
from __future__ import annotations
import typing

from gunicorn.app.base import BaseApplication

from fastapi_bike_sharing.app.views import BIKE_DEMAND_APP
from fastapi_bike_sharing.settings import APP_SETTINGS


if typing.TYPE_CHECKING:
    import fastapi


class GunicornApplication(BaseApplication):
    def load_config(self: GunicornApplication) -> None:
        _options: dict[str, str | int] = {
            "worker_class": "uvicorn.workers.UvicornWorker",
            "bind": f"0.0.0.0:{APP_SETTINGS.port}",
            "workers": APP_SETTINGS.workers,
        }
        for key, value in _options.items():
            if key in self.cfg.settings and value is not None:
                self.cfg.set(key.lower(), value)

    def load(self: GunicornApplication) -> fastapi.FastAPI:
        return BIKE_DEMAND_APP


if __name__ == "__main__":
    GunicornApplication().run()
