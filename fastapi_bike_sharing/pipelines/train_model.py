"""Train bike sharing model."""
import typing

import joblib
import pandas as pd
from loguru import logger
from sklearn import ensemble, model_selection

from fastapi_bike_sharing import settings
from fastapi_bike_sharing.model import features


if typing.TYPE_CHECKING:
    import pathlib


TEST_SIZE: typing.Final[float] = 0.2
RANDOM_FOREST_PARAMS: typing.Final[dict[str, int]] = {"random_state": 0, "n_estimators": 100}


def train_bike_demand_model() -> None:
    load_data_path: pathlib.Path = settings.RAW_DATA_PATH
    bike_demand_df: pd.DataFrame = pd.read_csv(load_data_path)
    logger.info(f"Training data loaded from {load_data_path!s}")

    feature_extractor: features.BikeFeatureExtractor = features.BikeFeatureExtractor(
        feature_column_order=features.FEATURE_COLUMN_ORDER,
        categorical_features=features.CATEGORICAL_FEATURES,
        target_columns=features.TARGET_COLUMNS,
    )

    feature_df: pd.DataFrame = feature_extractor.fit_transform(bike_demand_df)

    save_extractor_path: pathlib.Path = settings.FEATURE_EXTRACTOR_PATH
    feature_extractor.save(save_extractor_path)
    logger.info(f"Feature extractor saved to {save_extractor_path!s}")

    x: pd.DataFrame = feature_df.drop(columns=feature_extractor.target_columns)
    y: pd.DataFrame = feature_df[list(feature_extractor.target_columns)]

    target_column: str
    for target_column in feature_extractor.target_columns:
        x_train, x_test, y_train, y_test = model_selection.train_test_split(x, y[target_column], test_size=TEST_SIZE)
        regressor = ensemble.RandomForestRegressor(**RANDOM_FOREST_PARAMS)
        regressor.fit(x_train, y_train)

        model_path: pathlib.Path = settings.build_model_path(target_column)
        joblib.dump(regressor, model_path)
        logger.info(f"Model for {target_column!r} target saved to {model_path!s}.")


if __name__ == "__main__":
    train_bike_demand_model()
