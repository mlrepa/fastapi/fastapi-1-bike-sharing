"""Loading bike sharing data from the source."""
from __future__ import annotations
import io
import typing
import zipfile

import pandas as pd
import requests
from loguru import logger

from fastapi_bike_sharing import settings
from fastapi_bike_sharing.model.features import Col


if typing.TYPE_CHECKING:
    import pathlib


def download_bike_sharing_data(destination_path: pathlib.Path) -> None:
    """Download the Bike Sharing dataset from UCI machine learning repository.

    More information about the dataset can be found in UCI machine learning repository:
    https://archive.ics.uci.edu/ml/datasets/bike+sharing+dataset

    Acknowledgement: Fanaee-T, Hadi, and Gama, Joao,
    'Event labeling combining ensemble detectors and background knowledge',
    Progress in Artificial Intelligence (2013): pp. 1-15, Springer Berlin Heidelberg
    """
    logger.info(f"Loading bike sharing data from the source: {settings.DATA_SOURCE_URL}")
    byte_content: bytes | None = requests.get(settings.DATA_SOURCE_URL, timeout=settings.DATA_SOURCE_TIMEOUT).content

    with zipfile.ZipFile(io.BytesIO(byte_content)) as arc:
        raw_data: pd.DataFrame = pd.read_csv(
            arc.open(settings.DATA_FILE_NAME),
            header=0,
            sep=",",
            parse_dates=[Col.DATE],
        )

    raw_data.to_csv(destination_path, index=False)
    logger.info(f"Data downloaded to file: {destination_path}")


if __name__ == "__main__":
    download_bike_sharing_data(destination_path=settings.RAW_DATA_PATH)
