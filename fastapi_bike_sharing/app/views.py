"""All project end-points lie here."""
import typing

import fastapi
from anyio import to_thread
from loguru import logger

from fastapi_bike_sharing.app import data_models
from fastapi_bike_sharing.model.demand_service import BikeDemandService
from fastapi_bike_sharing.settings import APP_SETTINGS


BIKE_DEMAND_APP: typing.Final = fastapi.FastAPI(
    title=APP_SETTINGS.app_title,
    version=APP_SETTINGS.current_version,
    docs_url=APP_SETTINGS.docs_url,
    openapi_url=f"{APP_SETTINGS.api_prefix}/openapi.json",
)


@BIKE_DEMAND_APP.on_event("startup")
def startup() -> None:
    """Load and check demand model response."""
    service: BikeDemandService = BikeDemandService().prepare(data_models.TEST_PAYLOAD)
    test_demand: list[int] = service.predict_demand()
    if not all(isinstance(d, int) and d >= 0 for d in test_demand):
        raise RuntimeError("Demand model responded with unexpected values. Expected only integers greater than zero.")
    logger.info("Demand model warmed up successfully.")


@BIKE_DEMAND_APP.post(f"{APP_SETTINGS.api_prefix}/demand/", summary="Predict bike sharing demand")
async def bike_demand_main_endpoint(
    request_payload: data_models.BikeDemandRequest,
    demand_service: typing.Annotated[BikeDemandService, fastapi.Depends(BikeDemandService)],
) -> data_models.BikeDemandResponse:
    return data_models.BikeDemandResponse(
        target_type=request_payload.target_type,
        params=request_payload.params,
        predicted_demand=await to_thread.run_sync(demand_service.prepare(request_payload).predict_demand),
    )


@BIKE_DEMAND_APP.get(f"{APP_SETTINGS.api_prefix}/info/", summary="Get demand and weather types")
async def bike_demand_types() -> data_models.DemandTypeResponse:
    return data_models.DemandTypeResponse()


@BIKE_DEMAND_APP.get(f"{APP_SETTINGS.api_prefix}/health/", summary="Regular healthcheck api")
async def check_health_of_service() -> data_models.HealthCheckResponse:
    return data_models.HealthCheckResponse()
