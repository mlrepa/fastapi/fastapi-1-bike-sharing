"""Simple FastAPI application."""

import fastapi
import pandas as pd
import pydantic
import uvicorn
from fastapi.responses import HTMLResponse, JSONResponse, Response
from loguru import logger

from fastapi_bike_sharing import settings
from fastapi_bike_sharing.app import data_models
from fastapi_bike_sharing.model.demand_model import BikeDemandModel


class Features(pydantic.BaseModel):
    """Features model."""

    features: str = pydantic.Field(
        example="""
        {
            "dteday":{"0":"2012-12-31","1":"2012-12-31"},
            "season":{"0":1,"1":1},
            "yr":{"0":1,"1":1},
            "mnth":{"0":12,"1":12},
            "hr":{"0":22,"1":23},
            "holiday":{"0":0,"1":0},
            "weekday":{"0":1,"1":1},
            "workingday":{"0":1,"1":1},
            "weathersit":{"0":1,"1":1},
            "temp":{"0":0.26,"1":0.26},
            "atemp":{"0":0.2727,"1":0.2727},
            "hum":{"0":0.56,"1":0.65},
            "windspeed":{"0":0.1343,"1":0.1343},
            "casual":{"0":13,"1":12},
            "registered":{"0":48,"1":37},
            "cnt":{"0":61,"1":49}
        }
        """,
    )


APP = fastapi.FastAPI()
MODEL: BikeDemandModel = BikeDemandModel(
    target_type=data_models.TargetCol.TOTAL_COUNT,
    feature_extractor_path=settings.FEATURE_EXTRACTOR_PATH,
)


@APP.get("/")
def index() -> HTMLResponse:
    return HTMLResponse("<h1><i>FastAPI for Bike Demand Prediction</i></h1>")


@APP.post("/predict")
def predict(response: Response, features_item: Features) -> JSONResponse:
    try:
        features: pd.DataFrame = pd.read_json(features_item.features)
        features["predictions"] = MODEL.batch_predict(features)
        # Return JSON with predictions dataframe serialized to JSON string
        return JSONResponse(content={"predictions": features.to_json()})
    except Exception as e:  # noqa: BLE001
        response.status_code = 500
        logger.error(str(e), exc_info=True)
        return JSONResponse(content={"error_msg": str(e)})


if __name__ == "__main__":
    uvicorn.run(APP, host="0.0.0.0", port=8099)  # noqa: S104
