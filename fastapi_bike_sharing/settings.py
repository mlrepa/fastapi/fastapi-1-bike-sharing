"""Project and application settings."""
import pathlib
import typing

import pydantic
import toml
from loguru import logger
from pydantic_settings import BaseSettings, SettingsConfigDict


PROJECT_DIR: typing.Final = pathlib.Path(__file__).parent.parent
PYPROJECT_PATH: typing.Final[pathlib.Path] = PROJECT_DIR / "pyproject.toml"

DATA_DIR: typing.Final[pathlib.Path] = PROJECT_DIR / "data"
MODEL_DIR: typing.Final[pathlib.Path] = PROJECT_DIR / "models"
RAW_DATA_PATH: typing.Final[pathlib.Path] = DATA_DIR / "bike_sharing.csv"
FEATURE_EXTRACTOR_PATH: typing.Final[pathlib.Path] = MODEL_DIR / "feature_extractor.pkl"


def build_model_path(model_type: str) -> pathlib.Path:
    return MODEL_DIR / f"{model_type}_model.joblib"


def _parse_version_from_toml_file(default_value: str) -> str:
    try:
        pyproject_obj: dict[str, dict[str, dict[str, str]]] = toml.loads(PYPROJECT_PATH.read_text())
        return pyproject_obj["tool"]["poetry"]["version"]
    except (toml.TomlDecodeError, KeyError, FileNotFoundError) as exc:
        logger.warning(f"Cant parse version from pyproject. Trouble {exc}")
    return default_value


DATA_SOURCE_URL: typing.Final = "https://archive.ics.uci.edu/static/public/275/bike+sharing+dataset.zip"
DATA_SOURCE_TIMEOUT: typing.Final = 10
DATA_FILE_NAME: typing.Final = "hour.csv"


class BikeDemandSettings(BaseSettings):
    app_title: typing.Literal["Bike demand API"] = "Bike demand API"
    service_name: typing.Literal["bike-demand-service"] = "bike-demand-service"
    workers: typing.Annotated[int, pydantic.conint(gt=0, lt=16)] = pydantic.Field(
        2,
        description="application server workers count.",
    )
    port: typing.Annotated[int, pydantic.conint(gt=1_000, lt=60_000)] = pydantic.Field(
        8077,
        description="binding port",
    )
    cache_size: typing.Annotated[int, pydantic.conint(gt=0)] = pydantic.Field(
        10_000,
        description=("LRU cache size for bike deamnd requests. "),
    )
    api_prefix: typing.Annotated[
        str,
        pydantic.BeforeValidator(lambda possible_value: f"/{possible_value.strip('/')}"),
    ] = pydantic.Field("/api/", description="API's URL prefix")
    docs_url: str = pydantic.Field("/docs/", description="documentation (swagger) URL prefix")
    current_version: typing.Annotated[str, pydantic.BeforeValidator(_parse_version_from_toml_file)] = ""

    model_config = SettingsConfigDict(env_prefix="bike_demand_")


APP_SETTINGS: BikeDemandSettings = BikeDemandSettings()
